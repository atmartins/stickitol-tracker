const path = require('path');

module.exports = {
  devtool: 'sourcemaps',
  context: path.resolve(__dirname, 'src'),
  entry: {
    index: ['./index.jsx', './index.html']
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: 'babel-loader',
        include: [
          /src/
        ]
      },
      {
        test: /.html$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]'
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: [
      '.js',
      '.jsx'
    ]
  }
};
