import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Stage from './Stage';

ReactDOM.render(<Stage />, document.getElementById('app'));
