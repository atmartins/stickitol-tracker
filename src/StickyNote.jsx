import * as React from 'react';
const StickyNote = ({id, title}) => (<div className="sticky-note">Sticky {id} {title}</div>)
export default StickyNote
