import * as React from 'react';
import Fetcher from './Fetcher';
import StickyNote from './StickyNote';

export default class Stage extends React.Component {
  constructor () {
    super();
    // After this, state is immutable. Use setState() to modify it.
    this.state = {
      stickies: []
    }
  }

  loadStickies = () => {
    Fetcher.getStickies()
      // Object literal shorthand
      .then(stickies => this.setState({ stickies }))
  }

  render () {
    //Note: "className" instead of "class"
    //Note: parens implicitly return
    return (
      <div className="stage">
        <button onClick={this.loadStickies}>Load Stickies</button>
        <div className="sticky-notes">
          {this.state.stickies.map( (sticky) => (<StickyNote key={sticky.id} {...sticky} />) )}
        </div>
      </div>
    );
  }
}
